import React, { Component } from 'react'
import { 
    Row, 
    Col, 
    Card, 
    CardHeader, 
    CardTitle, 
    CardBody, 
    Table,
    Spinner
} from 'reactstrap'
import {connect} from 'react-redux'
import { getListPesanan } from '../../actions/PesananAction'
import { numberWithCommas } from 'utils'
import { Pesanans } from 'components'

class ListPesanan extends Component {

    componentDidMount(){
        this.props.dispatch(getListPesanan())
    }

    render() {
        const { getListPesananResult, getListPesananLoading, getListPesananError} = this.props
        return (
            <div className="content">
                <Row>
                    <Col md="12">
                        <Card>
                            <CardHeader>
                                <CardTitle tag="h4">Master Pesanan</CardTitle>
                            </CardHeader>
                            <CardBody>
                                <Table>
                                    <thead className="text-primary">
                                        <tr>
                                            <th>Tanggal</th>
                                            <th>Pesanan</th>
                                            <th>Status</th>
                                            <th>Total Harga</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            {getListPesananResult ? (
                                                    Object.keys(getListPesananResult).map((key) =>(
                                                        <tr key={key}>
                                                            <td>
                                                              <p>{getListPesananResult[key].tanggal}</p>
                                                              <p>({getListPesananResult[key].order_id})</p>
                                                             </td>
                                                          <td>
                                                              <Pesanans pesanans={getListPesananResult[key].pesanans}/> 
                                                          </td>
                                                          <td>{getListPesananResult[key].status}</td>
                                                          <td align="right">
                                                              <p>Total Harga : Rp. {numberWithCommas(getListPesananResult[key].totalHarga)}</p>
                                                              <p>Ongkir : Rp. {numberWithCommas(getListPesananResult[key].ongkir)}</p>
                                                              <p>
                                                                  <strong>
                                                                  Total : Rp. {numberWithCommas(getListPesananResult[key].totalHarga + getListPesananResult[key].ongkir)}
                                                                  </strong>
                                                              </p>
                                                           </td>
                                                           <td>
                                                               <a href={getListPesananResult[key].url} className="btn btn-primary" >
                                                                   <i className="nc-icon nc-money-coins"> </i> Midtrans
                                                               </a>
                                                           </td>
                                                        </tr>
                                                    ))
                                                ): getListPesananLoading ? (
                                                    //spinner loading
                                                    <tr>
                                                        <td colSpan="6" align="center"><Spinner color="primary" /></td>
                                                    </tr>
                                                ):getListPesananError ? (
                                                    //error
                                                    <tr>
                                                        <td colSpan="6" align="center">{getListPesananError}</td>
                                                    </tr>
                                                ) : (
                                                    <tr>
                                                        <td colSpan="6" align="center">Data Kosong</td>
                                                    </tr>
                                                )}
                                        </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    getListPesananLoading: state.PesananReducers.getListPesananLoading,
    getListPesananResult: state.PesananReducers.getListPesananResult,
    getListPesananError: state.PesananReducers.getListPesananError,
})

export default connect(mapStateToProps, null)(ListPesanan)
