import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row,
    Button,
    Spinner
} from 'reactstrap'
import swal from 'sweetalert'
import DefaultImage from '../../assets/img/default-image.jpg'
import { getListLiga } from '../../actions/LigaAction'
import { uploadJersey, tambahJersey } from '../../actions/JerseyAction'

class TambahJersey extends Component {
    constructor(props) {
        super(props)

        this.state = {
            image1: DefaultImage,
            image2: DefaultImage,
            imageToDB1: false,
            imageToDB2: false,

            nama: "",
            harga: 0,
            berat: 0,
            jenis: "",
            ukurans: ["S", "M", "L", "XL", "XXL"],
            ukuranSelected: [],
            ready: true,
            liga: ""
        }
    }

    componentDidMount() {
        this.props.dispatch(getListLiga())
    }

    componentDidUpdate(prevProps){
        const {uploadJerseyResult, tambahJerseyResult} = this.props

        if(uploadJerseyResult && prevProps.uploadJerseyResult !== uploadJerseyResult){
            this.setState({
                [uploadJerseyResult.imageToDB] : uploadJerseyResult.image
            })

            swal("Sukses", "Gambar Berhasil Di Upload", "success")
        }

        if(tambahJerseyResult && prevProps.tambahJerseyResult !== tambahJerseyResult){
            swal("Sukses", "Tambah Jersey Sukses Dibuat", "success")
            this.props.history.push('/admin/jersey')
        }
    }

    handelChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handelCheck = (event) => {
        // console.log("Event", event.target.check);
        const checked = event.target.checked
        const value = event.target.value

        if(checked){
            //jika user ceklist ukuran
            //isi state array ukuran selected
            this.setState({
                ukuranSelected: [...this.state.ukuranSelected, value]
            })
        }else{
            //jika user menghapus ceklist ukuran
            const ukuranBaru = this.state.ukuranSelected
                .filter((ukuran) => ukuran !== value)
                .map((filterUkuran) => {
                    return filterUkuran
                })

                this.setState({
                    ukuranSelected: ukuranBaru
                })
        }

    }

    handelImage = (event, imageToDB) => {
        // console.log("Event : ", event.target.files);
        if(event.target.files && event.target.files[0]){
            const gambar = event.target.files[0]
            this.setState({
                [event.target.name]: URL.createObjectURL(gambar),
            })
            this.props.dispatch(uploadJersey(gambar, imageToDB))
        }
    }

    handelSubmit = (event) => {
        const { berat, harga, nama, liga, ukuranSelected, jenis, imageToDB1, imageToDB2 } = this.state
        event.preventDefault()
        if( berat && harga && nama && liga && ukuranSelected && jenis && imageToDB1 && imageToDB2 ){
            //action
            this.props.dispatch(tambahJersey(this.state))
        }else{
            swal('Failed', 'Maaf semua form wajib diisi', 'error')
        }
    }

    render() {
        const {
            image1,
            image2,
            imageToDB1,
            imageToDB2,
            nama,
            harga,
            berat,
            jenis,
            ukurans,
            ready,
            liga,
        } = this.state
        const { getListLigaResult, tambahJerseyLoading } = this.props
        return (
            <div className="content">
                <Row>
                    <Col>
                        <Link to="/admin/jersey" className="btn btn-primary">
                            Kembali
                        </Link>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Card>
                            <CardHeader tag="h4">Tambah Jersey</CardHeader>
                            <CardBody>
                                <form onSubmit={(event) => this.handelSubmit(event)}>
                                    <Row>
                                        <Col md={6}>
                                            <Row>
                                                <Col>
                                                    <img src={image1} width={300} alt="Foto Jersey (Depan)" />
                                                    <FormGroup>
                                                        <label>Foto Jersey Depan</label>
                                                        <Input 
                                                            type="file" 
                                                            name="image1"  
                                                            onChange={
                                                                (event) => this.handelImage(event, "imageToDB1")
                                                            }
                                                        />
                                                    </FormGroup>
                                                    {image1 !== DefaultImage ? (
                                                        //selesai upload /proses upload
                                                        imageToDB1 ? (
                                                            <p><i className="nc-icon nc-check-2"></i>Sudah Upload</p>
                                                        ):(
                                                            <p><i className="nc-icon nc-user-run"></i>Proses Upload</p>
                                                        )

                                                    ) : (
                                                        //belum upload
                                                        <p><i className="nc-icon nc-cloud-upload-94"></i>Belum Upload</p>
                                                     
                                                    )}
                                                </Col>
                                                <Col>
                                                    <img src={image2} width={300} alt="Foto Jersey (Belakang)" />
                                                    <FormGroup>
                                                        <label>Foto Jersey Belakang</label>
                                                        <Input 
                                                            type="file" 
                                                            name="image2" 
                                                            onChange={
                                                                (event) => this.handelImage(event, "imageToDB2")
                                                            }/>
                                                    </FormGroup>

                                                    {image2 !== DefaultImage ? (
                                                        //selesai upload /proses upload
                                                        imageToDB2 ? (
                                                            <p><i className="nc-icon nc-check-2"></i>Sudah Upload</p>
                                                        ):(
                                                            <p><i className="nc-icon nc-user-run" ></i>Proses Upload</p>
                                                        )

                                                    ) : (
                                                        //belum upload
                                                        <p><i className="nc-icon nc-cloud-upload-94"></i>Belum Upload</p>
                                                     
                                                    )}
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col md={6}>
                                            <FormGroup>
                                                <label>Nama Jersey</label>
                                                <Input 
                                                    type="text" 
                                                    value={nama} 
                                                    name="nama" 
                                                    onChange={(event) => this.handelChange(event)} 
                                                />
                                            </FormGroup>

                                            <Row>
                                                <Col md={6}>
                                                    <FormGroup>
                                                        <label>Liga</label>
                                                        <Input 
                                                            type="select" 
                                                            name="liga"
                                                            value={liga} 
                                                            onChange={(event) => this.handelChange(event)} 
                                                        >
                                                            <option value="">--Pilih--</option>
                                                            {Object.keys(getListLigaResult).map((key) => (
                                                                <option value={key} key={key}>
                                                                    {getListLigaResult[key].namaLiga}
                                                                </option>
                                                            ))}
                                                        </Input>
                                                    </FormGroup>
                                                </Col>
                                                <Col md={6}>
                                                    <FormGroup>
                                                        <label>Harga (Rp.)</label>
                                                        <Input 
                                                            type="number" 
                                                            value={harga} 
                                                            name="harga" 
                                                            onChange={(event) => this.handelChange(event)} 
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={6}>
                                                    <FormGroup>
                                                        <label>Berat (kg)</label>
                                                        <Input 
                                                            type="number" 
                                                            value={berat} 
                                                            name="berat" 
                                                            onChange={(event) => this.handelChange(event)} 
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                <Col md={6}>
                                                    <FormGroup>
                                                        <label>jenis</label>
                                                        <Input 
                                                            type="text" 
                                                            value={jenis} 
                                                            name="jenis" 
                                                            onChange={(event) => this.handelChange(event)}  
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={6}>
                                                    <label>Ukuran</label>
                                                    <FormGroup check>
                                                        {ukurans.map((ukuran, index) => (
                                                            <Label key={index} check className="mr-2">
                                                                <Input type="checkbox" value={ukuran} onChange={(event) => this.handelCheck(event)} />
                                                                {ukuran}
                                                                <span className="form-check-sign">
                                                                    <span className="check"></span>
                                                                </span>
                                                            </Label>
                                                        ))}
                                                    </FormGroup>
                                                </Col>
                                                <Col md={6}>
                                                    <FormGroup>
                                                        <label>Ready</label>
                                                        <Input type="select" name="ready" value={ready} onChange={(event) => this.handelChange(event)} >
                                                            <option value={true}> Ada </option>
                                                            <option value={false}> kosong </option>
                                                        </Input>
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                        {tambahJerseyLoading ?(
                                            <Button 
                                            type="submit" 
                                            color="primary" 
                                            className="float-right"
                                            disabled
                                        >
                                            <Spinner size="sm" color="light"/> Loading ...
                                        </Button>
                                        ) : (
                                        <Button 
                                            type="submit" 
                                            color="primary" 
                                            className="float-right"
                                        >
                                            Submit
                                        </Button>
                                        )}
                                        </Col>
                                    </Row>
                                </form>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    getListLigaLoading: state.LigaReducers.getListLigaLoading,
    getListLigaResult: state.LigaReducers.getListLigaResult,
    getListLigaError: state.LigaReducers.getListLigaError,

    uploadJerseyLoading: state.JerseyReducers.uploadJerseyLoading,
    uploadJerseyResult: state.JerseyReducers.uploadJerseyResult,
    uploadJerseyError: state.JerseyReducers.uploadJerseyError,

    tambahJerseyLoading: state.JerseyReducers.tambahJerseyLoading,
    tambahJerseyResult: state.JerseyReducers.tambahJerseyResult,
    tambahJerseyError: state.JerseyReducers.tambahJerseyError,
})

export default connect(mapStateToProps, null)(TambahJersey)
