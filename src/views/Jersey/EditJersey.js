import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row,
    Button,
    Spinner
} from 'reactstrap'
import swal from 'sweetalert'
import DefaultImage from '../../assets/img/default-image.jpg'
import { getListLiga } from '../../actions/LigaAction'
import { uploadJersey, updateJersey, getDetailJersey } from '../../actions/JerseyAction'

class EditJersey extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            image1: DefaultImage,
            image2: DefaultImage,
            imageToDB1: false,
            imageToDB2: false,
            imageLama1: false,
            imageLama2: false,

            nama: "",
            harga: 0,
            berat: 0,
            jenis: "",
            ukurans: ["S", "M", "L", "XL", "XXL"],
            ukuranSelected: [],
            ready: true,
            liga: "",
            editUkuran: false
        }
    }

    componentDidMount() {
        this.props.dispatch(getListLiga())
        this.props.dispatch(getDetailJersey(this.props.match.params.id))
    }

    componentDidUpdate(prevProps) {
        const { uploadJerseyResult, updateJerseyResult, getDetailJerseyResult } = this.props

        if (uploadJerseyResult && prevProps.uploadJerseyResult !== uploadJerseyResult) {
            this.setState({
                [uploadJerseyResult.imageToDB]: uploadJerseyResult.image
            })

            swal("Sukses", "Gambar Berhasil Di Upload", "success")
        }

        if (updateJerseyResult && prevProps.updateJerseyResult !== updateJerseyResult) {
            swal("Sukses", updateJerseyResult , "success")
            this.props.history.push('/admin/jersey')
        }

        if (getDetailJerseyResult && prevProps.getDetailJerseyResult !== getDetailJerseyResult) {
            this.setState({
                image1: getDetailJerseyResult.gambar[0],
                image2: getDetailJerseyResult.gambar[1],
                imageLama1: getDetailJerseyResult.gambar[0],
                imageLama2: getDetailJerseyResult.gambar[1],
                nama: getDetailJerseyResult.nama,
                harga: getDetailJerseyResult.harga,
                berat: getDetailJerseyResult.berat,
                jenis: getDetailJerseyResult.jenis,
                ukuranSelected: getDetailJerseyResult.ukuran,
                ready: getDetailJerseyResult.ready,
                liga: getDetailJerseyResult.liga,

            })
        }
    }

    handelChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handelCheck = (event) => {
        // console.log("Event", event.target.check);
        const checked = event.target.checked
        const value = event.target.value

        if (checked) {
            //jika user ceklist ukuran
            //isi state array ukuran selected
            this.setState({
                ukuranSelected: [...this.state.ukuranSelected, value]
            })
        } else {
            //jika user menghapus ceklist ukuran
            const ukuranBaru = this.state.ukuranSelected
                .filter((ukuran) => ukuran !== value)
                .map((filterUkuran) => {
                    return filterUkuran
                })

            this.setState({
                ukuranSelected: ukuranBaru
            })
        }

    }

    handelImage = (event, imageToDB) => {
        // console.log("Event : ", event.target.files);
        if (event.target.files && event.target.files[0]) {
            const gambar = event.target.files[0]
            this.setState({
                [event.target.name]: URL.createObjectURL(gambar),
            })
            this.props.dispatch(uploadJersey(gambar, imageToDB))
        }
    }

    handelSubmit = (event) => {
        const { berat, harga, nama, liga, ukuranSelected, jenis} = this.state
        event.preventDefault()
        if (berat && harga && nama && liga && ukuranSelected && jenis) {
            //action
            this.props.dispatch(updateJersey(this.state))
        } else {
            swal('Failed', 'Maaf semua form wajib diisi', 'error')
        }
    }

    editUkuran = () => {
        this.setState({
            editUkuran: true,
            ukuranSelected: []
        })
    }

    render() {
        const {
            image1,
            image2,
            imageToDB1,
            imageToDB2,
            nama,
            harga,
            berat,
            jenis,
            ukurans,
            ready,
            liga,
            ukuranSelected,
            editUkuran,
            imageLama1,
            imageLama2
        } = this.state
        const { getListLigaResult, updateJerseyLoading } = this.props
        return (
            <div className="content">
                <Row>
                    <Col>
                        <Link to="/admin/jersey" className="btn btn-primary">
                            Kembali
                        </Link>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Card>
                            <CardHeader tag="h4">Edit Jersey</CardHeader>
                            <CardBody>
                                <form onSubmit={(event) => this.handelSubmit(event)}>
                                    <Row>
                                        <Col md={6}>
                                            <Row>
                                                <Col>
                                                    <img src={image1} width={300} alt="Foto Jersey (Depan)" />
                                                    <FormGroup>
                                                        <label>Foto Jersey Depan</label>
                                                        <Input
                                                            type="file"
                                                            name="image1"
                                                            onChange={
                                                                (event) => this.handelImage(event, "imageToDB1")
                                                            }
                                                        />
                                                    </FormGroup>
                                                    {image1 !== imageLama1 ? (
                                                        //selesai upload /proses upload
                                                        imageToDB1 ? (
                                                            <p><i className="nc-icon nc-check-2"></i>Sudah Upload</p>
                                                        ) : (
                                                            <p><i className="nc-icon nc-user-run"></i>Proses Upload</p>
                                                        )

                                                    ) : (
                                                        //belum upload
                                                        <p><i className="nc-icon nc-cloud-upload-94"></i>Belum Upload</p>

                                                    )}
                                                </Col>
                                                <Col>
                                                    <img src={image2} width={300} alt="Foto Jersey (Belakang)" />
                                                    <FormGroup>
                                                        <label>Foto Jersey Belakang</label>
                                                        <Input
                                                            type="file"
                                                            name="image2"
                                                            onChange={
                                                                (event) => this.handelImage(event, "imageToDB2")
                                                            } />
                                                    </FormGroup>

                                                    {image2 !== imageLama2 ? (
                                                        //selesai upload /proses upload
                                                        imageToDB2 ? (
                                                            <p><i className="nc-icon nc-check-2"></i>Sudah Upload</p>
                                                        ) : (
                                                            <p><i className="nc-icon nc-user-run" ></i>Proses Upload</p>
                                                        )

                                                    ) : (
                                                        //belum upload
                                                        <p><i className="nc-icon nc-cloud-upload-94"></i>Belum Upload</p>

                                                    )}
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col md={6}>
                                            <FormGroup>
                                                <label>Nama Jersey</label>
                                                <Input
                                                    type="text"
                                                    value={nama}
                                                    name="nama"
                                                    onChange={(event) => this.handelChange(event)}
                                                />
                                            </FormGroup>

                                            <Row>
                                                <Col md={6}>
                                                    <FormGroup>
                                                        <label>Liga</label>
                                                        <Input
                                                            type="select"
                                                            name="liga"
                                                            value={liga}
                                                            onChange={(event) => this.handelChange(event)}
                                                        >
                                                            <option value="">--Pilih--</option>
                                                            {Object.keys(getListLigaResult).map((key) => (
                                                                <option value={key} key={key}>
                                                                    {getListLigaResult[key].namaLiga}
                                                                </option>
                                                            ))}
                                                        </Input>
                                                    </FormGroup>
                                                </Col>
                                                <Col md={6}>
                                                    <FormGroup>
                                                        <label>Harga (Rp.)</label>
                                                        <Input
                                                            type="number"
                                                            value={harga}
                                                            name="harga"
                                                            onChange={(event) => this.handelChange(event)}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={6}>
                                                    <FormGroup>
                                                        <label>Berat (kg)</label>
                                                        <Input
                                                            type="number"
                                                            value={berat}
                                                            name="berat"
                                                            onChange={(event) => this.handelChange(event)}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                <Col md={6}>
                                                    <FormGroup>
                                                        <label>jenis</label>
                                                        <Input
                                                            type="text"
                                                            value={jenis}
                                                            name="jenis"
                                                            onChange={(event) => this.handelChange(event)}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={6}>
                                                    <label>Ukuran Yang Tersedia Sekarang : (
                                                            {ukuranSelected.map((item, index) => (
                                                            <strong key={index}> {item} </strong>
                                                            ))}
                                                        )
                                                     </label>
                                                     {editUkuran ? (
                                                         <>
                                                            <FormGroup check>
                                                                {ukurans.map((ukuran, index) => (
                                                                    <Label key={index} check className="mr-2">
                                                                        <Input type="checkbox" value={ukuran} onChange={(event) => this.handelCheck(event)} />
                                                                        {ukuran}
                                                                        <span className="form-check-sign">
                                                                            <span className="check"></span>
                                                                        </span>
                                                                    </Label>
                                                                ))}
                                                            </FormGroup>
                                                            <Button color="primary" size="sm" onClick={() => this.setState({editUkuran:false})}>
                                                                Selesai Edit Ukuran
                                                            </Button>
                                                         </>
                                                     ) : (
                                                         <Button color="primary" size="sm" onClick={() => this.editUkuran()}>
                                                             Edit Ukuran
                                                         </Button>
                                                     )}
                                                    
                                                </Col>
                                                <Col md={6}>
                                                    <FormGroup>
                                                        <label>Ready</label>
                                                        <Input type="select" name="ready" value={ready} onChange={(event) => this.handelChange(event)} >
                                                            <option value={true}> Ada </option>
                                                            <option value={false}> kosong </option>
                                                        </Input>
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            {updateJerseyLoading ? (
                                                <Button
                                                    type="submit"
                                                    color="primary"
                                                    className="float-right"
                                                    disabled
                                                >
                                                    <Spinner size="sm" color="light" /> Loading ...
                                                </Button>
                                            ) : (
                                                <Button
                                                    type="submit"
                                                    color="primary"
                                                    className="float-right"
                                                >
                                                    Submit
                                                </Button>
                                            )}
                                        </Col>
                                    </Row>
                                </form>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    getListLigaLoading: state.LigaReducers.getListLigaLoading,
    getListLigaResult: state.LigaReducers.getListLigaResult,
    getListLigaError: state.LigaReducers.getListLigaError,

    uploadJerseyLoading: state.JerseyReducers.uploadJerseyLoading,
    uploadJerseyResult: state.JerseyReducers.uploadJerseyResult,
    uploadJerseyError: state.JerseyReducers.uploadJerseyError,

    getDetailJerseyLoading: state.JerseyReducers.getDetailJerseyLoading,
    getDetailJerseyResult: state.JerseyReducers.getDetailJerseyResult,
    getDetailJerseyError: state.JerseyReducers.getDetailJerseyError,

    updateJerseyLoading: state.JerseyReducers.updateJerseyLoading,
    updateJerseyResult: state.JerseyReducers.updateJerseyResult,
    updateJerseyError: state.JerseyReducers.updateJerseyError,
})

export default connect(mapStateToProps, null)(EditJersey)
