import React, { Component } from 'react'
import { 
    Row, 
    Col, 
    Card, 
    CardHeader, 
    CardTitle, 
    CardBody,
    FormGroup,
    Input,
    Button,
    Spinner
} from 'reactstrap'
import {connect} from 'react-redux'
import { Link } from 'react-router-dom'
import DefaultImage from '../../assets/img/default-image.jpg'
import swal from 'sweetalert'
import { getDetailLiga,updateLiga } from 'actions/LigaAction'

class EditLiga extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             id: this.props.match.params.id,
             imageLama: DefaultImage,
             image: DefaultImage,
             imageToDB: false,
             namaLiga: "",
        }
    }

    handelChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handelImage = (event) => {
        // console.log("Event : ", event.target.files);
        if(event.target.files && event.target.files[0]){
            const gambar = event.target.files[0]
            this.setState({
                image: URL.createObjectURL(gambar),
                imageToDB: gambar
            })
        }
    }

    handelSubmit = (event) =>{
        // console.log("Event : ", event);
        const { namaLiga } = this.state
        event.preventDefault()
        if(namaLiga){
            //prosses lanjut ke action firebase
            this.props.dispatch(updateLiga(this.state))
        }else{
            //alert
            swal("Failed!", "Maaf Nama Liga harus diisi", "error")
        }
    }
    
    componentDidUpdate(prevProps){
        const { updateLigaResult, getDetailLigaResult } = this.props
        if(updateLigaResult && prevProps.updateLigaResult !== updateLigaResult){
            swal("Sukses", "Liga Sukses Diupdate", "success")
            this.props.history.push('/admin/liga')
        }

        if(getDetailLigaResult && prevProps.getDetailLigaResult !== getDetailLigaResult){
            this.setState({
                image: getDetailLigaResult.image,
                namaLiga: getDetailLigaResult.namaLiga,
                imageLama: getDetailLigaResult.image
            })
        }
    }

    componentDidMount(){
        this.props.dispatch(getDetailLiga(this.props.match.params.id))
    }

    render() {
        const { image, namaLiga } = this.state
        const { updateLigaLoading } = this.props
        return (
            <div className="content">
                <Row>
                    <Col>
                        <Link to="/admin/liga" className="btn btn-primary"> Kembali </Link>
                    </Col>
                </Row>
                <Row>
                    <Col md="12">
                        <Card>
                            <CardHeader>
                                <CardTitle tag="h4">Edit Liga</CardTitle>
                            </CardHeader>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <img src={image} width="200" alt="Logo Liga"/>
                                    </Col>
                                </Row>
                                <form onSubmit={(event) => this.handelSubmit(event)}>
                                    <Row>

                                        <Col md={6}>
                                            <FormGroup>
                                                <label>
                                                    Logo Liga
                                                </label>
                                                <Input 
                                                    type="file" 
                                                    onChange={(event) => this.handelImage(event)}
                                                />
                                            </FormGroup>
                                        </Col>

                                        <Col md={6}>
                                            <FormGroup>
                                                <label>
                                                    Nama Liga
                                                </label>
                                                <Input 
                                                    type="text" 
                                                    value={namaLiga} 
                                                    name="namaLiga" 
                                                    onChange={(event) => this.handelChange(event)}
                                                />
                                            </FormGroup>
                                        </Col>

                                    </Row>

                                    <Row>
                                        <Col>
                                        {updateLigaLoading ?(
                                            <Button color="primary" type="submit" disabled>
                                              <Spinner size="sm" color="light"/>  Loading
                                            </Button>
                                        ) : (
                                            <Button color="primary" type="submit">
                                              Submit
                                            </Button>
                                        )}
                                            
                                        </Col>
                                    </Row>
                                </form>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    updateLigaLoading: state.LigaReducers.updateLigaLoading,
    updateLigaResult: state.LigaReducers.updateLigaResult,
    updateLigaError: state.LigaReducers.updateLigaError,

    getDetailLigaLoading: state.LigaReducers.getDetailLigaLoading,
    getDetailLigaResult: state.LigaReducers.getDetailLigaResult,
    getDetailLigaError: state.LigaReducers.getDetailLigaError,
})

export default connect(mapStateToProps, null)(EditLiga)
