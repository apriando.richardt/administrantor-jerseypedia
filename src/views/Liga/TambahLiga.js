import React, { Component } from 'react'
import { 
    Row, 
    Col, 
    Card, 
    CardHeader, 
    CardTitle, 
    CardBody,
    FormGroup,
    Input,
    Button,
    Spinner
} from 'reactstrap'
import {connect} from 'react-redux'
import { Link } from 'react-router-dom'
import DefaultImage from '../../assets/img/default-image.jpg'
import swal from 'sweetalert'
import { tambahLiga } from 'actions/LigaAction'

class TambahLiga extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             image: DefaultImage,
             imageToDB: false,
             namaLiga: "",
        }
    }

    handelChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handelImage = (event) => {
        // console.log("Event : ", event.target.files);
        if(event.target.files && event.target.files[0]){
            const gambar = event.target.files[0]
            this.setState({
                image: URL.createObjectURL(gambar),
                imageToDB: gambar
            })
        }
    }

    handelSubmit = (event) =>{
        // console.log("Event : ", event);
        const { imageToDB, namaLiga } = this.state
        event.preventDefault()
        if(imageToDB && namaLiga){
            //prosses lanjut ke action firebase
            this.props.dispatch(tambahLiga(this.state))
        }else{
            //alert
            swal("Failed!", "Maaf Nama Liga dan Logo Liga harus diisi", "error")
        }
    }
    
    componentDidUpdate(prevProps){
        const { tambahLigaResult } = this.props
        if(tambahLigaResult && prevProps.tambahLigaResult !== tambahLigaResult){
            swal("Sukses", "Liga Sukses Dibuat", "success")
            this.props.history.push('/admin/liga')
        }
    }

    render() {
        const { image, namaLiga } = this.state
        const { tambahLigaLoading } = this.props
        return (
            <div className="content">
                <Row>
                    <Col>
                        <Link to="/admin/liga" className="btn btn-primary"> Kembali </Link>
                    </Col>
                </Row>
                <Row>
                    <Col md="12">
                        <Card>
                            <CardHeader>
                                <CardTitle tag="h4">Tambah Liga</CardTitle>
                            </CardHeader>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <img src={image} width="200" alt="Logo Liga"/>
                                    </Col>
                                </Row>
                                <form onSubmit={(event) => this.handelSubmit(event)}>
                                    <Row>

                                        <Col md={6}>
                                            <FormGroup>
                                                <label>
                                                    Logo Liga
                                                </label>
                                                <Input 
                                                    type="file" 
                                                    onChange={(event) => this.handelImage(event)}
                                                />
                                            </FormGroup>
                                        </Col>

                                        <Col md={6}>
                                            <FormGroup>
                                                <label>
                                                    Nama Liga
                                                </label>
                                                <Input 
                                                    type="text" 
                                                    value={namaLiga} 
                                                    name="namaLiga" 
                                                    onChange={(event) => this.handelChange(event)}
                                                />
                                            </FormGroup>
                                        </Col>

                                    </Row>

                                    <Row>
                                        <Col>
                                        {tambahLigaLoading ?(
                                            <Button color="primary" type="submit" disabled>
                                              <Spinner size="sm" color="light"/>  Loading
                                            </Button>
                                        ) : (
                                            <Button color="primary" type="submit">
                                              Submit
                                            </Button>
                                        )}
                                            
                                        </Col>
                                    </Row>
                                </form>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    tambahLigaLoading: state.LigaReducers.tambahLigaLoading,
    tambahLigaResult: state.LigaReducers.tambahLigaResult,
    tambahLigaError: state.LigaReducers.tambahLigaError,
})

export default connect(mapStateToProps, null)(TambahLiga)
