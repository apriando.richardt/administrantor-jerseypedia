import React, { Component } from 'react'
import { Card, CardHeader, CardBody , Col, Row, Button, Spinner} from 'reactstrap'
import Logo from '../../assets/img/logoUtama.svg'
import {connect} from 'react-redux'
import {updatePesanan} from '../../actions/PesananAction'

class Finish extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             order_id: '',
             transaction_status: ''
        }
    }

    componentDidMount(){
        // ?order_id=ORDER-101-1628087186&status_code=201&transaction_status=pending
        let search = window.location.search
        let params = new URLSearchParams(search)

        const order_id = params.get('order_id')
        const transaction_status = params.get('transaction_status')

        if(order_id){
            this.setState({
                order_id: order_id,
                transaction_status: transaction_status
            })

            //masuk ke action update history
            this.props.dispatch(updatePesanan(order_id, transaction_status))
        }
    }

    toHistory(){
        window.ReactNativeWebView.postMessage('Selesai')
    }
    
    render() {
        const {order_id, transaction_status} = this.state
        const {updateStatusLoading} = this.props
        return (
            <div>
                <Row className="justify-content-center mt-5">
                    {updateStatusLoading ? (
                        <Spinner color="primary"/>
                    ):(
                        <Col md="4" className="mt-5">
                            <img src={Logo} className="rounded mx-auto d-block" alt="Logo"/>
                            <Card>
                                <CardHeader tag="h4" align="center">Selamat Transaksi Anda Berhasil</CardHeader>
                                <CardBody className="text-center">
                                <p>{transaction_status === "pending" && "Untuk Selanjutnya Harap Selesaikan Pembayaran nya jika belum bayar, dan silahkan update Status Pembayaranya di Halaman History"}</p>
                                <p>ORDER ID : {order_id}</p>
                                <p>STATUS TRANSAKSI : {transaction_status === "settlement" || transaction_status === "capture" ? "lunas" : transaction_status}</p>
                                <Button color="primary" type="submit" onClick={() => this.toHistory()}>
                                    Lanjutkan
                                </Button>
                                </CardBody>
                            </Card>
                        </Col>
                    )}
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    updateStatusLoading: state.PesananReducers.updateStatusLoading
})

export default connect(mapStateToProps, null)(Finish)
