
import React, { Component } from 'react'
import { Card, CardHeader, CardBody , Col, Row, FormGroup, Label, Input, Button, Spinner } from 'reactstrap'
import swal from 'sweetalert'
import Logo from '../../assets/img/logoUtama.svg'
import {connect} from 'react-redux'
import {checkLogin, loginUser} from '../../actions/AuthAction'


class Login extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             email: "",
             password: ""
        }
    }

    componentDidMount(){
        this.props.dispatch(checkLogin(this.props.history))
    }

    handelChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handelSubmit = (event) => {
        const {email, password} = this.state
        event.preventDefault()

        if(email && password){
            //action login
            this.props.dispatch(loginUser(email, password))
        }else{
            swal("Failed", "Maaf Email dan Password Harus diisi", "error")
        }
    }

    componentDidUpdate(prevProps){
        const { loginResult, checkLoginResult } = this.props
        if(loginResult && prevProps.loginResult !== loginResult){
            this.props.history.push('/admin/dashboard')
        }

        if(checkLoginResult && prevProps.checkLoginResult !== checkLoginResult){
            this.props.history.push('/admin/dashboard')
        }
    }
    
    render() {
        const {email, password} = this.state
        const { loginLoading } = this.props
        return (
            <Row className="justify-content-center mt-5">
                <Col md="4" className="mt-5">
                    <img src={Logo} className="rounded mx-auto d-block" alt="Logo"/>
                    <Card>
                        <CardHeader tag="h4">Login</CardHeader>
                        <CardBody>
                            <form onSubmit={(event) => this.handelSubmit(event)}>
                                <FormGroup>
                                    <Label for="email">Email Address</Label>
                                    <Input 
                                        type="email" 
                                        value={email} 
                                        name="email" 
                                        placeholder="Enter Email" 
                                        onChange={(event) => this.handelChange(event)}
                                    />
                                </FormGroup>

                                <FormGroup>
                                    <Label for="password">Passowrd</Label>
                                    <Input 
                                        type="password" 
                                        value={password} 
                                        name="password" 
                                        placeholder="Enter password" 
                                        onChange={(event) => this.handelChange(event)}
                                    />
                                </FormGroup>
                                {loginLoading ? (
                                    <Button color="primary" type="submit" disabled>
                                        <Spinner size="sm" color="light"/>  Loading
                                    </Button>
                                ) : (
                                    <Button color="primary" type="submit"> Login </Button>
                                )}
                                
                            </form>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        )
    }
}

const mapStateToProps = (state) => ({
    loginLoading: state.AuthReducers.loginLoading,
    loginResult: state.AuthReducers.loginResult,
    loginError: state.AuthReducers.loginError,

    checkLoginLoading: state.AuthReducers.checkLoginLoading,
    checkLoginResult: state.AuthReducers.checkLoginResult,
    checkLoginError: state.AuthReducers.checkLoginError,
})

export default connect(mapStateToProps, null)(Login)
