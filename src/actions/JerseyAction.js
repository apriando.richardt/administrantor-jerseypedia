import FIREBASE from '../config/FIREBASE'
import { dispatchLoading, dispatchSuccess, dispatchError } from '../utils'

export const  GET_LIST_JERSEY = "GET_LIST_JERSEY"
export const  UPLOAD_JERSEY = "UPLOAD_JERSEY"
export const  TAMBAH_JERSEY = "TAMBAH_JERSEY"
export const  GET_DETAIL_JERSEY = "GET_DETAIL_JERSEY"
export const  UPDATE_JERSEY = "UPDATE_JERSEY"
export const  DELETE_JERSEY = "DELETE_JERSEY"

export const getListJersey = () =>{
    return (dispatch) => {
        dispatchLoading(dispatch, GET_LIST_JERSEY)

        FIREBASE.database()
                .ref('jerseys')
                .once('value', (querySnapshot) => {

                    // Hasil
                    // console.log("Data : ",querySnapshot.val());
                    let data = querySnapshot.val()
                    // let dataItem = {...data}
                    
                    dispatchSuccess(dispatch, GET_LIST_JERSEY, data)
                })
                .catch((error) =>{
                    // Error
                    dispatchError(dispatch, GET_LIST_JERSEY, error)
                    alert(error)
                })
    }
}

export const uploadJersey = (gambar, imageToDB) => {
    return (dispatch) =>{
        dispatchLoading(dispatch, UPLOAD_JERSEY)

            var uploadTask = FIREBASE.storage()
                .ref('jerseys')
                .child(gambar.name)
                .put(gambar);

            uploadTask.on('state_changed', function(snapshot){
                console.log(snapshot);
            }, function(error) {
                dispatchError(dispatch, UPLOAD_JERSEY, error)
                alert(error)
            }, function() {

                uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
                // console.log('File available at', downloadURL);
                    const dataBaru = {
                        image: downloadURL,
                        imageToDB: imageToDB
                    }
                    dispatchSuccess(dispatch, UPLOAD_JERSEY, dataBaru)
                });
            });
    }
}

export const tambahJersey = (data) => {
    return (dispatch) => {
        dispatchLoading(dispatch, TAMBAH_JERSEY)

        const dataBaru = {
            gambar: [data.imageToDB1, data.imageToDB2],
            nama: data.nama,
            harga: data.harga,
            berat: data.berat,
            jenis: data.jenis,
            ready: data.ready,
            ukuran: data.ukuranSelected,
            liga: data.liga
        }

        FIREBASE.database()
            .ref('jerseys')
            .push(dataBaru)
            .then((response) =>{
                dispatchSuccess(dispatch, TAMBAH_JERSEY, response)
            })
            .catch((error) => {
                 // Error
                 dispatchError(dispatch, TAMBAH_JERSEY, error)
                 alert(error)
            })
    }
}

export const getDetailJersey = (id) =>{
    return (dispatch) => {
        dispatchLoading(dispatch, GET_DETAIL_JERSEY)

        FIREBASE.database()
                .ref('jerseys/'+id)
                .once('value', (querySnapshot) => {

                    // Hasil
                    // console.log("Data : ",querySnapshot.val());
                    let data = querySnapshot.val()
                    // let dataItem = {...data}
                    
                    dispatchSuccess(dispatch, GET_DETAIL_JERSEY, data)
                })
                .catch((error) =>{
                    // Error
                    dispatchError(dispatch, GET_DETAIL_JERSEY, error)
                    alert(error)
                })
    }
}

export const updateJersey = (data) => {
    return (dispatch) => {
        dispatchLoading(dispatch, UPDATE_JERSEY)

        const dataBaru = {
            gambar: [
                data.imageToDB1 ? data.imageToDB1 : data.imageLama1, 
                data.imageToDB2 ? data.imageToDB2 : data.imageLama2
            ],
            nama: data.nama,
            harga: data.harga,
            berat: data.berat,
            jenis: data.jenis,
            ready: data.ready,
            ukuran: data.ukuranSelected,
            liga: data.liga
        }

        FIREBASE.database()
            .ref('jerseys/'+data.id)
            .update(dataBaru)
            .then((response) =>{

                if(data.imageToDB1) {
                    //hapus data storage lama image 1
                    var desertRef = FIREBASE.storage().refFromURL(data.imageLama1)
                    desertRef
                        .delete()
                        .catch(function (error) {
                             // Error
                            dispatchError(dispatch, UPDATE_JERSEY, error)
                        })
                }

                if(data.imageToDB2) {
                    //hapus data storage lama image 1
                    var desertRef2 = FIREBASE.storage().refFromURL(data.imageLama2)
                    desertRef2
                        .delete()
                        .catch(function (error) {
                             // Error
                            dispatchError(dispatch, UPDATE_JERSEY, error)
                        })
                }
                // console.log("sukses : ", response);
                dispatchSuccess(dispatch, UPDATE_JERSEY, "Update Jersey Sukses")
            })
            .catch((error) => {
                 // Error
                 dispatchError(dispatch, UPDATE_JERSEY, error)
                 alert(error)
            })
    }
}

export const deleteJersey = (images, id) => {
    // console.log("Data Image Jersy Delete : ", images);
    // console.log("Data Id Jersy Delete : ", id);
    return (dispatch) =>{
        dispatchLoading(dispatch, DELETE_JERSEY)

        var desertRef = FIREBASE.storage().refFromURL(images[0])
        desertRef
            .delete()
            .then(function() {
                var desertRef2 = FIREBASE.storage().refFromURL(images[1])

                desertRef2
                    .delete()
                    .then(function() {
                        // hapus realtime database
                        FIREBASE.database()
                            .ref('jerseys/'+id)
                            .remove()
                            .then(function () {
                                console.log("data id jersey :", id);
                                dispatchSuccess(dispatch, DELETE_JERSEY, "Jersey Berhasil di Hapus")
                            })
                            .catch(function(error){
                                dispatchError(dispatch, DELETE_JERSEY, error)
                            })
                    })
                    .catch(function (error) {
                        dispatchError(dispatch, DELETE_JERSEY, error)
                    })
            })
            .catch(function (error) {
                 // Error
                dispatchError(dispatch, DELETE_JERSEY, error)
            })
    }
}