import FIREBASE from '../config/FIREBASE'
import { dispatchLoading, dispatchSuccess, dispatchError } from '../utils'

export const  GET_LIST_PESANAN = "GET_LIST_PESANAN"
export const  UPDATE_PESANAN = "UPDATE_PESANAN"

export const getListPesanan = () =>{
    return (dispatch) => {
        dispatchLoading(dispatch, GET_LIST_PESANAN)

        FIREBASE.database()
                .ref('histories')
                .once('value', (querySnapshot) => {

                    // Hasil
                    // console.log("Data : ",querySnapshot.val());
                    let data = querySnapshot.val()
                    // let dataItem = {...data}
                    
                    dispatchSuccess(dispatch, GET_LIST_PESANAN, data)
                })
                .catch((error) =>{
                    // Error
                    dispatchError(dispatch, GET_LIST_PESANAN, error)
                    alert(error)
                })
    }
}


export const updatePesanan = (order_id, transaction_status) =>{
    return (dispatch) => {
        dispatchLoading(dispatch, UPDATE_PESANAN)

        const status = transaction_status === "settlement" || transaction_status === "capture" ? "lunas" : transaction_status

        FIREBASE.database()
                .ref('histories')
                .child(order_id)
                .update({
                    status: status
                })
                .then((response) => {
                    dispatchSuccess(dispatch, UPDATE_PESANAN, response ? response : [])
                })
                .catch((error) =>{
                    // Error
                    dispatchError(dispatch, UPDATE_PESANAN, error)
                    alert(error)
                })
    }
}
