import firebase from "firebase"

firebase.initializeApp({
    apiKey: "AIzaSyAxy96evKKp0t7bYnxMHPhganf-gCd5n_M",
    authDomain: "jerseypedia-134db.firebaseapp.com",
    projectId: "jerseypedia-134db",
    storageBucket: "jerseypedia-134db.appspot.com",
    messagingSenderId: "399230291981",
    appId: "1:399230291981:web:812ae8d707242f58fbc544"
})

const FIREBASE = firebase

export default FIREBASE 
