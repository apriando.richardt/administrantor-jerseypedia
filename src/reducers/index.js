import {combineReducers} from 'redux'
import LigaReducers from './liga'
import JerseyReducers from './jersey'
import AuthReducers from './auth'
import PesananReducers from './pesanan'

export default combineReducers({
    LigaReducers,
    JerseyReducers,
    AuthReducers,
    PesananReducers
})